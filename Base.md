# 1. Базовый инструментарий

PostgreSQL — программа, которая относится к классу систем управления базами данных.
Когда эта программа выполняется, мы называем ее сервером  PostgreSQL или экземпляром сервера. 
Данные, которыми управляет PostgreSQL, хранятся в базах данных. 
Один экземпляр PostgreSQL одновременно работает с несколькими базами данных. Этот набор баз данных называется кластером баз данных. 

![clusterDB](/images/clusterDB.png)


*********
#### Устанорвка из пакетов

*Create the file repository configuration:*
*Добавляем репозиторий*

`sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'`

*Import the repository signing key:*

*Скачиваем ключи*

`wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -`

*Update the package lists*

*Обновляем пакеты* 

`sudo apt-get update`

*Install the latest version of PostgreSQL.*

*If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':*

*Устанавливаем последнюю весию*

`sudo apt-get -y install postgresql`

*Проверяем запущенную службу*

`sudo systemctl status postgresql`

*Если не запущена служба, запустим*

`sudo systemctl start postgresql`

*********
#### Устанорвка из исходного кода

*Обновляем, устанавливаем обновления, зависимости, необходимые пакеты для сборки*

```
apt update && apt upgrade
Install required packages
    dnf install readline-devel
    dnf install zlib-devel
    dnf install gcc
    dnf install make
```
`sudo apt-get install build-essential libreadline-dev zlib1g-dev flex bison libxml2-dev libxslt-dev libssl-dev libxml2-utils xsltproc ccache`

 
*Создаем новый каталог* 

`mkdir -p /local/apps/postgresql/pgsql115/`
 
*Скачиваем по пути* 
www.postgresql.org => downloads => Source code => File browser => 11.5

winscp .gz file  VM
 
 *Разархивируем* 
 ```bash
gunzip postgresql-11.5.tar.gz
tar -xvf postgresql-11.5.tar
```

*Конфигурируем и устанавливаем* 

```bash
./configure --prefix=/local/apps/postgresql/pgsql115/ --with-pgport=5432
make          <= To start the build
make install    <= This will install PostgreSQl into the directory specified in ./configure step
useradd -d /home/postgres/ postgres
passwd postgres
mkdir -p /local/apps/postgresql/pgsql115/data
chown postgres /local/apps/postgresql/pgsql115/data
 
su - postgres
/local/apps/postgresql/pgsql115/bin/initdb -D /local/apps/postgresql/pgsql115/data
/local/apps/postgresql/pgsql115/bin/postgres -D /local/apps/postgresql/pgsql115/data
 ```

*Проверяем работающий postgres*

`ps -ef | grep postgres`
 
*Устанавливаем переменные окружения* 

```
vi .bash_profile
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/local/apps/postgresql/pgsql115/lib
export PATH=$PATH:/local/apps/postgresql/pgsql115/bin
```

https://edu.postgrespro.ru/dba1-13/dba1_01_tools_install.html

*********
#### Устанорвка при помощи docker

```Docker
version: '3.5'
services:
  postgres:
    container_name: postgres_container
    image: postgres
    environment:
      POSTGRES_USER: ${POSTGRES_USER:-postgres}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-changeme}
      PGDATA: /data/postgres
    volumes:
       - postgres:/data/postgres
    ports:
      - "5432:5432"
    networks:
      - postgres
    restart: unless-stopped
  
  pgadmin:
    container_name: pgadmin_container
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL:-pgadmin4@pgadmin.org}
      PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD:-admin}
      PGADMIN_CONFIG_SERVER_MODE: 'False'
    volumes:
       - pgadmin:/var/lib/pgadmin

    ports:
      - "${PGADMIN_PORT:-5050}:80"
    networks:
      - postgres
    restart: unless-stopped

networks:
  postgres:
    driver: bridge

volumes:
    postgres:
    pgadmin:
```
*Перезагружает конфигурация внутри docker контейнера)))*

`docker exec -it {postgres_container}  psql -U postgres -c "SELECT pg_reload_conf();"`

Проверка версии
*SELECT version();*

*********
# Управление кластером баз данных 
Утилита **pg_ctl** отвечает за начальной инициализацию, остановку, получение статуса сервера, перечитывание параметров конфигурации переключение на реплику.

Для запуска сервера:

`pg_ctl start`


Для остановки сервера:

`pg_ctl stop`


Повторный запуск сервера

`pg_ctl restart`

Вывод состояния сервера

`pg_ctl status`

# PSQL

Программа **psql** — это терминальный **клиент**  для работы с PostgreSQL. Она позволяет интерактивно вводить запросы, передавать их в PostgreSQL и видеть результаты. Также запросы могут быть получены из файла или из аргументов командной строки. Кроме того, psql предоставляет ряд метакоманд и различные возможности, подобные тем, что имеются у командных оболочек, для облегчения написания скриптов и автоматизации широкого спектра задач.


#### Подключение к базе данных

> Если postgres уже установлен дополнительно устанавливать клиент не нужно

*Для установки клиента выполните команду*

`sudo apt install postgresql-client`
`sudo apt install postgresql-client-common`

*Пример подключения к северу и к базе данных*

`psql -U postgres -h 192.168.100.5 -p 5432`

`psql -d postgres -U postgres -h 192.168.100.5 -p 5432`

*Проверка информации о подключении*
```
postgres=# \conninfo
You are connected to database "postgres" as user "postgres" on host "192.168.100.5" at port "5432".
```

```
postgres=# help
You are using psql, the command-line interface to PostgreSQL.
Type:  \copyright for distribution terms
       \h for help with SQL commands
       \? for help with psql commands
       \g or terminate with semicolon to execute query
       \q to quit
```

Справка по синтаксису команды
```
postgres=# \h drop table
Command:     DROP TABLE
Description: remove a table
Syntax:
DROP TABLE [ IF EXISTS ] name [, ...] [ CASCADE | RESTRICT ]

URL: https://www.postgresql.org/docs/12/sql-droptable.html

```

Список всех баз данных
```
postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
(3 rows)

```

Получить дополнительную информацию о пространстве, которые занимают базы данных, и их описание **\l+**
```
postgres=# \l+
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   |  Size   | Tablespace |                Description                 
-----------+----------+----------+------------+------------+-----------------------+---------+------------+--------------------------------------------
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 |                       | 7453 kB | pg_default | default administrative connection database
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +| 7297 kB | pg_default | unmodifiable empty database
           |          |          |            |            | postgres=CTc/postgres |         |            | 
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +| 7525 kB | pg_default | default template for new databases
           |          |          |            |            | postgres=CTc/postgres |         |            | 
```

Для редактирования вывода команды \l применяется команда \x


| Command | 	Effect |
| --- | ----------- |
| \x off | 	В ширину |
| \x on | 	В длину |
| \x |	В предыдущее состояние |
| \x auto |	Удобный формат для терминала |


```
postgres=# \x
Expanded display is on.
postgres=# \l
List of databases
-[ RECORD 1 ]-----+----------------------
Name              | postgres
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.utf8
Ctype             | en_US.utf8
Access privileges | 
-[ RECORD 2 ]-----+----------------------
Name              | template0
Owner             | postgres
Encoding          | UTF8
Collate           | en_US.utf8
Ctype             | en_US.utf8
Access privileges | =c/postgres          +
                  | postgres=CTc/postgres

```

Подключение к базе данных
```
postgres=# \c test
psql (12.13 (Ubuntu 12.13-0ubuntu0.20.04.1), server 15.2 (Debian 15.2-1.pgdg110+1))
WARNING: psql major version 12, server major version 15.
         Some psql features might not work.
You are now connected to database "test" as user "postgres".
```
> Для переключения между базами используйте ту же команду

Для отображения таблиц используйте команду **\dt**
```
test=# \dt
           List of relations
 Schema |    Name    | Type  |  Owner   
--------+------------+-------+----------
 public | company    | table | postgres
 public | department | table | postgres
(2 rows)
```

Для отображения информации о столбцах **\d**
```
test=# \d company
                                Table "public.company"
 Column  |     Type      | Collation | Nullable |               Default               
---------+---------------+-----------+----------+-------------------------------------
 id      | integer       |           | not null | nextval('company_id_seq'::regclass)
 name    | text          |           | not null | 
 age     | integer       |           | not null | 
 address | character(50) |           |          | 
 salary  | real          |           |          | 
Indexes:
    "company_pkey" PRIMARY KEY, btree (id)
```

*********
#### Взаимодействие с ОС 

В psql можно выполнять команды оболочки:
```
test=# \! pwd
/home/xub

```

Установка переменной окружения операционной системы:
```
postgres=# \setenv TEST Hello
postgres=# \! echo $TEST
Hello
```

Запись вывода команды в файл с помощью опции **\o**
```
test=# \o test.log
test=# SELECT * FROM public.company;
test=# \! cat test.log
 id | name  | age |                      address                       | salary 
----+-------+-----+----------------------------------------------------+--------
  1 | Paul  |  32 | California                                         |  20000
  2 | Allen |  25 | Texas                                              |  15000
  3 | Teddy |  23 | Norway                                             |  20000
  4 | Mark  |  25 | Rich-Mond                                          |  65000
  5 | David |  27 | Texas                                              |  85000
  6 | Kim   |  22 | South-Hall                                         |  45000
  7 | James |  24 | Houston                                            |  10000
(7 rows)
```
> выход **q**

где **public** это представление

Представление — это способ задать ИМЯ для запроса SELECT на выборку данных и по этому ИМЕНИ быстро получать нужную «собранную» таблицу. То есть можно создать сложный запрос в базу данных и дать ему имя, чтобы каждый раз не расписывать полный SELECT

*********
#### Конфигурирование

В PostgreSQL существует большое количество параметров, влияющих на работу СУБД. Параметры позволяют управлять потреблением ресурсов, астраивать работу серверных процессов и т.д. Для установки параметров используются файлы конфигурации. Если не определено иное, значения, установленные в этих  файлах, действуют для всего экземпляра СУБД. Ряд параметров можно установить для сеансов в отдельной базе данных или для сеансов отдельного пользователя. Такие установки будут иметь предпочтение перед файлами конфигурации.

Основной конфигурационный файл — postgresql.conf.
Расположение файла задается при сборке PostgreSQL. По умолчанию файл располагается в каталоге с данными (PGDATA) /var/lib/pgpro/std-10/data., но пакетные дистрибутивы  обычно размещают этот файл в другом месте, в соответствии с правилами, принятыми в конкретной ОС.


```
postgres=# SHOW config_file;
          config_file           
--------------------------------
 /data/postgres/postgresql.conf
(1 row)
```

Пересматриваем конфигурацию

> если установлено из исходного кода

`pg_ctl reload`

> из пакетов 

```
pg_ctlcluster number_versio main reload
SELECT pg_reload_conf();
```


Чтобы отобразить все незакоментированные строки конфигурационного файла, можно обратиться к представлению pg_file_settings: 

```
postgres=# SELECT sourceline, name, setting, applied
postgres-# FROM pg_file_settings
postgres-# WHERE sourcefile LIKE '/data/postgres/postgresql.conf';
 sourceline |            name            |      setting       | applied 
------------+----------------------------+--------------------+---------
         60 | listen_addresses           | *                  | t
         65 | max_connections            | 100                | t
        127 | shared_buffers             | 128MB              | t
        150 | dynamic_shared_memory_type | posix              | t
        241 | max_wal_size               | 1GB                | t
        242 | min_wal_size               | 80MB               | t
        597 | log_timezone               | Etc/UTC            | t
        711 | datestyle                  | iso, mdy           | t
        713 | timezone                   | Etc/UTC            | t
        727 | lc_messages                | en_US.utf8         | t
        729 | lc_monetary                | en_US.utf8         | t
        730 | lc_numeric                 | en_US.utf8         | t
        731 | lc_time                    | en_US.utf8         | t
        734 | default_text_search_config | pg_catalog.english | t
(14 rows)
```

Действующие значения всех параметров доступны в представлении pg_settings. Вот что в нем содержится, например, для параметра work_mem: 

```
postgres=# SELECT name, setting, unit,
postgres-#   boot_val, reset_val,
postgres-#   source, sourcefile, sourceline,
postgres-#   pending_restart, context
postgres-# FROM pg_settings
postgres-# WHERE name = 'work_mem'\gx
-[ RECORD 1 ]---+---------
name            | work_mem
setting         | 4096
unit            | kB
boot_val        | 4096
reset_val       | 4096
source          | default
sourcefile      | 
sourceline      | 
pending_restart | f
context         | user
```
Параметр work_mem определяет объем памяти, который выделяется для выполнения таких операций, как сортировка или хеш-соединение. 


 Рассмотрим ключевые столбцы представления pg_settings:

    * name, setting, unit — название и значение параметра;
    * boot_val — значение по умолчанию;
    * reset_val — значение, которое восстановит команда RESET;
    * source — источник текущего значения параметра;
    * pending_restart — значение изменено в файле конфигурации, но для применения требуется перезапуск сервера.

Столбец context определяет действия, необходимые для применения параметра. Среди возможных значений:

    * internal — изменить нельзя, значение задано при установке;
    * postmaster — требуется перезапуск сервера;
    * sighup — требуется перечитать файлы конфигурации,
    * superuser — суперпользователь может изменить для своего сеанса;
    * user — любой пользователь может изменить для своего сеанса.


Если один и тот же параметр встречается в файле несколько раз, то устанавливается значение из последней считанной строки.

Запишем в конец postgresql.conf две строки с параметром work_mem: 

```
echo work_mem=12MB | sudo tee -a /data/postgres/postgresql.conf

work_mem=12MB

echo work_mem=12MB | sudo tee -a /data/postgres/postgresql.conf

work_mem=8MB


SELECT sourceline, name, setting, applied
FROM pg_file_settings
WHERE name = 'work_mem';

 sourceline |   name   | setting | applied 
------------+----------+---------+---------
        783 | work_mem | 12MB    | f
        784 | work_mem | 8MB     | t
(2 rows)


Перечитаем файлы конфигурации:

=> SELECT pg_reload_conf();

 pg_reload_conf 
----------------
 t
(1 row)

```

Посмотрим значение work_mem тойже командой

```
 SELECT name, setting, unit,
  boot_val, reset_val,
  source, sourcefile, sourceline,
  pending_restart, context
FROM pg_settings
WHERE name = 'work_mem'\gx

-[ RECORD 1 ]---+----------------------------------------
name            | work_mem
setting         | 8192
unit            | kB
boot_val        | 4096
reset_val       | 8192
source          | configuration file
sourcefile      | /data/postgres/postgresql.conf
sourceline      | 784
pending_restart | f
context         | user
```

*********
#### postgresql.conf

Последним считывается файл postgresql.auto.conf. Этот файл всегда располагается в каталоге данных (PGDATA). 
Этот файл не следует изменять вручную. Для его редактирования предназначена команда ALTER SYSTEM. По сути, ALTER SYSTEM  представляет собой SQL-интерфейс для управления параметрами конфигурации. Для применения изменений, сделанных командой ALTER SYSTEM, сервер должен перечитать конфигурационные файлы, как и в случае с изменением файла postgresql.conf. Содержимое обоих файлов (postgresql.conf и postgresql.auto.conf) можно  увидеть через представление pg_file_settings. А актуальные значения параметров — в представлении pg_settings.

```
postgres=# ALTER SYSTEM SET work_mem TO '16MB';
ALTER SYSTEM

SELECT pg_read_file('/data/postgres/postgresql.conf')
\g (tuples_only=on format=unaligned)
```

Для применения не забываем:

`SELECT pg_reload_conf();`

В результате

```
SELECT name, setting, unit,
  boot_val, reset_val,
  source, sourcefile, sourceline,
  pending_restart, context
FROM pg_settings
WHERE name = 'work_mem'\gx

-[ RECORD 1 ]---+-------------------------------------------------
name            | work_mem
setting         | 16384
unit            | kB
boot_val        | 4096
reset_val       | 16384
source          | configuration file
sourcefile      | /var/lib/postgresql/13/main/postgresql.auto.conf
sourceline      | 3
pending_restart | f
context         | user
```

Для удаления строк из postgresql.auto.conf используется команда ALTER SYSTEM RESET:

`ALTER SYSTEM RESET work_mem;`


*********
#### Установка параметров для текущего сеанса 

Для изменения параметров во время сеанса командной строки можно использовать команду **SET**
 
`SET work_mem TO '24MB';`

Используя функцию **set_config**
```
SELECT set_config('work_mem', '32MB', false);

 set_config 
------------
 32MB
(1 row)
```

Получить значение параметра можно разными способами: 

```
SHOW work_mem;

 work_mem 
----------
 32MB
(1 row)

SELECT current_setting('work_mem');

 current_setting 
-----------------
 32MB
(1 row)

SELECT name, setting, unit FROM pg_settings WHERE name = 'work_mem';

   name   | setting | unit 
----------+---------+------
 work_mem | 32768   | kB
(1 row)

```

*********
#### Утилита psql и обработка ошибок внутри транзакциях 

Утилита psql по умолчанию работает в режиме автоматической фиксации транзакций. Поэтому любая команда SQL выполняется в отдельной транзакции. 
Чтобы явно начать транзакцию, нужно выполнить команду **BEGIN**

`BEGIN;`

> Обратите внимание на то, что приглашение psql изменилось. Символ «звездочка» говорит о том, что транзакция сейчас активна.

test=*# CREATE TABLE t (id int);

> Предположим, мы случайно сделали ошибку в следующей команде: 

```
INSERTINTO t VALUES(1);

ERROR:  syntax error at or near "INSERTINTO"
LINE 1: INSERTINTO t VALUES(1);
        ^
```

Звездочка изменилась на восклицательный знак. Попробуем исправить команду: 
```
 INSERT INTO t VALUES(1);

ERROR:  current transaction is aborted, commands ignored until end of transaction block

```
PostgreSQL не умеет откатывать только одну команду транзакции

После коммита и выбора строк в таблице показывает что не существует таблицы в базе данных


Полностью вывод
```
test=# BEGIN;
BEGIN
test=# INSERTINTO t VALUES(1);
ERROR:  syntax error at or near "INSERTINTO"
LINE 1: INSERTINTO t VALUES(1);
        ^
test=# INSERT INTO t VALUES(1);
ERROR:  current transaction is aborted, commands ignored until end of transaction block
test=# COMMIT;
ROLLBACK
test=# SELECT * FROM t;
ERROR:  relation "t" does not exist
LINE 1: SELECT * FROM t;
                      ^
```

Установим переменную psql **ON_ERROR_ROLLBACK on** Теперь перед каждой командой транзакции неявно будет устанавливаться точка сохранения, а в случае ошибки будет происходить автоматический откат к этой точке.

`\set ON_ERROR_ROLLBACK on`

Практические задания ч1
1. Запустите psql и проверьте информацию о текущем подключении.
2. Выведите все строки таблицы pg_tables.
3. Установите команду «less -XS» для постраничного просмотра и еще раз выведите все строки pg_tables.
4. Приглашение по умолчанию показывает имя базы данных. Настройте приглашение так, чтобы дополнительно выводилась информация о пользователе: роль@база=#
5. Настройте psql так, чтобы для всех команд выводилась длительность выполнения. Убедитесь, что при повторном запуске эта настройка сохраняется

> HELP(1)
1. При запуске psql можно не указывать параметры подключения, будут действовать значения по умолчанию.
3. Настройку переменной окружения PSQL_PAGER можно сделать в файле .psqlrc. Используйте команду \setenv, чтобы установить  переменную окружения ОС. Это позволит использовать значение «less - XS» только при работе в psql. Для всех остальных команд ОС будут использоваться настройки в ОС (например, изprofile)
4. Описание настройки приглашения в документации: https://postgrespro.ru/docs/postgresql/13/app-psql#APP-PSQL-PROMPTING
5. Команду psql для вывода длительности выполнения запросов можно найти в документации или с помощью команды \?.

Практические задания ч2
1. Получите список параметров, для изменения которых требуется перезапуск сервера.
2. В файле postgresql.conf сделайте ошибку при изменении параметра max_connections. Перезапустите сервер. Убедитесь, что сервер не стартует, и проверьте журнал сообщений. Исправьте ошибку и запустите сервер.

> HELP(2)

2. Расположение файла postgresql.conf можно найти, посмотрев значение параметра config_file. Редактируйте файл postgresql.conf либо от имени владельца – 
пользователя postgres), либо с правами суперпользователя. В первом случае удобно открыть новое окно терминала и выполнить в нем команду: sudo su postgres
Во втором случае запускайте редактор из командной строки через команду sudo, например: sudo vim postgresql.conf


